package com.example.jdbcdemo.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;


import com.example.jdbcdemo.domain.Season;

public class SeasonManagerTest {

	SeasonManager seasonManager = new SeasonManager();
	
	private final static int NUMBER_1 = 1;
	private final static int YEAR_1 = 1995;
	
	@Test
	public void checkConnection(){
		assertNotNull(seasonManager.getConnection());
	}
	
	@Test
	public void checkAdding(){
		
		Season season = new Season(NUMBER_1, YEAR_1, null);
		
		seasonManager.clearSeason();
		assertEquals(1,seasonManager.addSeason(season));
		
		List<Season> seasons = seasonManager.getAllSeason();
		Season seasonRetrieved = seasons.get(0);
		
		assertEquals(NUMBER_1, seasonRetrieved.getSeasonNumber());
		assertEquals(YEAR_1, seasonRetrieved.getYearOfRlease());
		
	}

}
